package com.sundogsoftware.sparkstreaming.iot

import java.sql.Date

import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

object IOTExample {

  case class DeviceData(device: String, deviceType: String, signal: Double, time: Date)

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("IOTInfo")
      .getOrCreate()

    import spark.implicits._

    val schema : StructType = new StructType()
      .add("device", "string")
      .add("deviceType", "string")
      .add("signal", "double")
      .add("time", "date")

    val df: DataFrame = spark
      .readStream
      .schema(schema)
      .json("/Users/tl39to/workspaces/spark/SparkStreamingExamples/src/main/scala/com/sundogsoftware/sparkstreaming/iot/data")

    // streaming DataFrame with IOT device data with schema { device: string, deviceType: string, signal: double, time: string }
    val ds: Dataset[DeviceData] = df.as[DeviceData]    // streaming Dataset with IOT device data

    // Select the devices which have signal more than 10
    val signalingDevices: Dataset[Row] = df.select("device", "deviceType", "signal").where("signal > 10") // using untyped APIs
    //ds.filter(_.signal > 10).map(_.device)         // using typed APIs

    // Running count of the number of updates for each device type
    val signalingDevicesByType = signalingDevices.groupBy("deviceType").count()                          // using untyped API

    // Running average signal for each device type
    //import org.apache.spark.sql.expressions.scalalang.typed
    //ds.groupByKey(_.deviceType).agg(typed.avg(_.signal))    // using typed API

    //Count the number of devices with the same deviceType that have signal > 10
    val query = signalingDevicesByType.writeStream
      .outputMode("complete") //Complete is needed in order to aggregate
      .format("console")
      .start()

    while(true) {}
  }
}
