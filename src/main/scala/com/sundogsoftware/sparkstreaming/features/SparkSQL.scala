package com.sundogsoftware.sparkstreaming.features

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.spark.streaming.{Seconds, StreamingContext}

//{"device": "iot","deviceType": "antenna","signal": 0,"time": "2014-06-01"}
case class IOTData(device: String, deviceType: String, signal: Long, time: String)

object SparkSQL {

  def main(args: Array[String]): Unit = {

    import org.apache.spark.sql._

    val spark = SparkSession.builder().master("local[*]").appName("sparkSQL").getOrCreate()
    val streamingContext = new StreamingContext(spark.sparkContext, Seconds(1))

    val objectMapper = new ObjectMapper()

    streamingContext
      .textFileStream("file:///Users/TL39TO/workspaces/spark/SparkStreamingExamples/src/main/scala/com/sundogsoftware/sparkstreaming/iot/data")
      //.map(jsonLine => objectMapper.readValue(jsonLine, classOf[IOTData]))
      .foreachRDD(rdd => {
        import spark.sqlContext.implicits._

        println(rdd.first())
        val frame: DataFrame = spark.sqlContext.createDataFrame(rdd, classOf[IOTData])
        val doubleSignal : DataFrame = frame.withColumn("signal", $"signal" * 2d)
        val filterBySignalIntensity: DataFrame = doubleSignal.filter($"signal" > 100d)
        val select : DataFrame = filterBySignalIntensity.select($"device", $"deviceType", $"signal")

        val rows: Array[Row] = select.take(10)
        select.show()
      }
    )

    streamingContext.start()
    streamingContext.awaitTermination()


    //select.explain(true)

  }
}
