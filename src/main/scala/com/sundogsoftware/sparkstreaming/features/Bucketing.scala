package com.sundogsoftware.sparkstreaming.features

import org.apache.spark.sql.SparkSession

object Bucketing {

  import org.apache.spark.sql.SaveMode

  def main(args: Array[String]): Unit = {

    implicit val spark = SparkSession
      .builder
      .appName("PrintCollection")
      .master("local[*]")
      //.config("spark.sql.warehouse.dir", "file:///C:/temp") // Necessary to work around a Windows bug in Spark 2.0.0; omit if you're not on Windows.
      .config("spark.sql.streaming.checkpointLocation", "./checkpoint")
      .getOrCreate()

    spark.range(10e4.toLong).write.mode(SaveMode.Overwrite).saveAsTable("t10e4")
    spark.range(10e6.toLong).write.mode(SaveMode.Overwrite).saveAsTable("t10e6")

    // Bucketing is enabled by default
    // Let's check it out anyway
    assert(spark.sessionState.conf.bucketingEnabled, "Bucketing disabled?!")

    // Make sure that you don't end up with a BroadcastHashJoin and a BroadcastExchange
    // For that, let's disable auto broadcasting
    spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

    val tables = spark.catalog.listTables.where("name LIKE 't10e%'")
    tables.show

    val t4 = spark.table("t10e4")
    val t6 = spark.table("t10e6")

    assert(t4.count == 10e4)
    assert(t6.count == 10e6)

    // trigger execution of the join query
    t4.join(t6, "id").foreach(_ => ())

    while(true) {}
  }
}
