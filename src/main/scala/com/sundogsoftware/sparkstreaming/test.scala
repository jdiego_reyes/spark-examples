package com.sundogsoftware.sparkstreaming

import scala.collection.immutable.TreeMap

object Test {

  def main(args: Array[String]): Unit = {
    val list = List(1,2,3)
    val list2 = List(1,2,3,4,5,6,7,8,9,10)

    println(lastOfList(list))
    println(addElem(list, 4))
    println(drop(3, list2))
    println(listTuples(list, list2))
    println(reduceList(list2, _+_))
    println(notContainedElements(list.toSet, list2.toSet))
    println(treeMaps(TreeMap()))
    println(List(1,2,3) ++ List.fill(3)('_'))
  }


  def lastOfList[T](list: List[T]) : T = {
    list match {
      case h :: Nil => h
      case _ :: tail => lastOfList(tail)
      case _ => throw new NoSuchElementException
    }
  }

  def addElem[T](list: List[T], elem: T) : List[T] = {
    list :+ elem
  }


  def drop[T](n: Int, list: List[T]) : List[T] = {
    list.filter(elem => (list.indexOf(elem) + 1) % n != 0)
  }

  def listTuples[T](list1: List[T], list2: List[T]) : List[(T, T)] = {
    list1.zip(list2)
  }

  def reduceList(list: List[Int], reduce: (Int, Int) => Int): Int = {
    list.reduce(reduce)
  }

  def notContainedElements(set: Set[Int], set2: Set[Int]): Set[Int] = {
    //O(n^n)
    set.filter(elem => !set2.contains(elem))
  }

  def treeMaps(map: TreeMap[String, Int]) = {
    TreeMap(("hello" -> 1), ("hello" -> 2), ("hello" -> null))
  }
}
