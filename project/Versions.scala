object Versions {

  lazy val sparkVersion = "2.3.4"

  object Twitter {
    lazy val twitterDepsVersion = "4.0.4"
  }

  object Connectors {
    lazy val cassandraConnectorVersion = "2.4.1"
  }
}
